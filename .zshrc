# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# SSH-agent
agent=$HOME/.ssh/agent.env
agent_load_env () { test -f "$agent" && . "$agent" >| /dev/null ; }
agent_start () {
  (umask 077; ssh-agent >| "$agent")
  . "$agent" >| /dev/null ; }

agent_load_env
# agent_run_state: 0=agent running w/ key; 1=agent w/o key; 2= agent not running
agent_run_state=$(ssh-add -l >| /dev/null 2>&1; echo $?)
if [ ! "$SSH_AUTH_SOCK" ] || [ $agent_run_state = 2 ]; then
  agent_start
  ssh-add
elif [ "$SSH_AUTH_SOCK" ] && [ $agent_run_state = 1 ]; then
  ssh-add
fi
unset agent

# XDG
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
XDG_RUNTIME_DIR=/run/user/$UID
if [ ! -w $XDG_RUNTIME_DIR ]; then
  echo "\$XDG_RUNTIME_DIR ($XDG_RUNTIME_DIR) not writable. Setting to /tmp." >&2
  XDG_RUNTIME_DIR=/tmp
fi
export XDG_RUNTIME_DIR

HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
unsetopt beep

autoload -Uz compinit
compinit

#DEFAULTS
export BROWSER=`which firefox`
export EDITOR=`which vim`
if hash nvim 2>/dev/null; then
export EDITOR=`which nvim`
fi
export GIT_EDITOR=$EDITOR
export VISUAL=$EDITOR
#DISABLE crtl+s in terminal
stty ixany
stty ixon -ixoff

#GO
export GOPATH=$HOME/.go
export PATH=$PATH:$GOPATH/bin:/usr/lib/go/bin
#RUST BIN
export PATH=$PATH:$HOME/.cargo/bin
#LOCAL PATH
export PATH=$PATH:$HOME/.local/bin
#NPM PATH
export PATH=$PATH:$HOME/.npm-global/bin

export CASE_SENSITIVE="true"
source $HOME/.aliases

# Vi mode in zsh
bindkey -v
KEYTIMEOUT=1

if hash starship 2>/dev/null; then
eval "$(starship init zsh)"
fi

if hash lsd 2>/dev/null; then
alias ls='lsd'
fi

if [ -z $DISPLAY ] && [ -n $XDG_VTNR ] && [ "$XDG_VTNR" -eq 1 ]; then
exec startx
fi

if [[ $(hostname) != "mini" && "$TMUX" == "" ]]; then
  WHOAMI=$(whoami)
  if tmux has-session -t $WHOAMI 2>/dev/null; then
    tmux attach-session -t $WHOAMI
  else
    tmux new-session -s $WHOAMI
  fi
fi
source $HOME/.localvars
