{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/release-22.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    npm-build-package.url = "github:serokell/nix-npm-buildpackage";
    neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";
    alejandra = {
      url = "github:kamadorueda/alejandra/main";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix = {
      url = "github:NixOS/nix/2.10.3";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {self, ...} @ inputs: let
    system = "x86_64-linux";
    lib = inputs.nixpkgs.lib;
    unstablePkgs = import inputs.unstable {
      inherit system;
    };
    overlay-module = {pkgs, ...}: {
      nixpkgs.config.allowUnfree = true;
      nixpkgs.overlays =
        [
          inputs.npm-build-package.overlays.default
          inputs.neovim-nightly-overlay.overlay
          inputs.nix.overlays.default
          (
            final: prev: {
              erigon = unstablePkgs.erigon;
            }
          )
        ]
        ++ (import ./overlays);
    };
    flakes = {pkgs, ...}: {
      nix = {
        extraOptions = ''
          experimental-features = nix-command flakes
          keep-outputs = true
          keep-derivations = true
        '';
        trustedUsers = ["root"];
        gc = {
          automatic = true;
          options = "--delete-older-than 10d";
        };
      };
    };
  in {
    nixosConfigurations.miniMin = lib.nixosSystem {
      inherit system;
      modules = [
      ];
    };
    nixosConfigurations.mini = lib.nixosSystem {
      inherit system;
      modules = [
        overlay-module
        ./nixos/configuration.nix
        flakes
        inputs.home-manager.nixosModules.home-manager
        {
          home-manager.useUserPackages = true;
          home-manager.useGlobalPkgs = true;
          home-manager.users.sebas = {pkgs, ...}: {
            home.file.".config/nvim/lua/debugging.lua".text = ''
              local extension_path = "${pkgs.vscode-extensions.vadimcn.vscode-lldb}/share/vscode/extensions/vadimcn.vscode-lldb/"
              return {
                adapter = require('rust-tools.dap').get_codelldb_adapter(
                  extension_path .. 'adapter/codelldb',
                  extension_path .. 'lldb/lib/liblldb.so'
                )
              }
            '';
            home.stateVersion = "22.05";
            services.gpg-agent.pinentryFlavor = ["gtk2"];
            programs = {
              lsd.enable = true;
              direnv.enable = true;
              direnv.nix-direnv.enable = true;
              z-lua = {
                enable = true;
                enableZshIntegration = true;
              };
              zsh = {
                enable = true;
                enableCompletion = true;
                enableAutosuggestions = true;
                initExtra = builtins.readFile ./.zshrc;
              };
            };
            home.packages = with pkgs; [
              inputs.alejandra.defaultPackage.${system}
              yaml-language-server
              vscode
              vimPlugins.markdown-preview-nvim
              nix
              gh
              cachix
              my-brave
              my-firefox
              my-google
              my-chromium
              libreoffice-fresh
              rofi
              gitFull
              bitwarden
              stremio
              rofi-rbw
              element-desktop
              signal-desktop
              tmux
              alacritty
              gopls
              starship
              vault
              consul
              terraform
              nomad
              vlc
              speedcrunch
              pavucontrol
              neovim
              delta
              yubikey-manager
              python39Full
              yubioath-desktop
              #zfs
              _1password-gui
              slack
              calibre
              aws
              go_1_18
              fd
              powertop
              ripgrep
              lazydocker
              radicle-upstream
              erigon
            ];

            systemd.user.services = {
              browser-whitelist = pkgs.browser-whitelist-service;
            };
          };
        }
      ];
    };
  };
}
