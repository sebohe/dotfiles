final: prev: let
  nodeModules = prev.mkNodeModules {
    src = ./.;
    pname = "my-node-modules";
    version = "1.0.0";
  };
  makeNodeScript = name: path:
    prev.writeScriptBin name ''
      #!/usr/bin/env sh
      export NODE_PATH="${nodeModules}/node_modules"
      exec ${prev.nodejs}/bin/node ${path} "$@"
    '';
  makeNodeService = {
    Description,
    ExecStart,
  }: {
    Unit = {
      inherit Description;
      After = ["graphical-session-pre.target"];
      PartOf = ["graphical-session.target"];
    };
    Install = {WantedBy = ["graphical-session.target"];};
    Service = {
      inherit ExecStart;
      Environment = ''NODE_PATH="${nodeModules}/node_modules"'';
      Restart = "always";
    };
  };
in {
  browser-whitelist-service = makeNodeService {
    Description = "Browser Whitelist daemon";
    ExecStart = "${prev.nodejs}/bin/node ${./browser_whitelist.js} ${./whitelist.json}";
  };
  roam-backup = makeNodeScript "roam_backup.js" ./roam_backup.js;
}
