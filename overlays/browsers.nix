final: prev:
with prev; {
  # Todo convert this into a browser => port map
  my-firefox =
    prev.writeScriptBin "firefox"
    ''      #!/usr/bin/env bash
            ${prev.firefox-devedition-bin}/bin/firefox-devedition --remote-debugging-port=9222 "$@"
    '';
  my-brave =
    prev.writeScriptBin "brave"
    ''      #!/usr/bin/env bash
            ${prev.brave}/bin/brave --remote-debugging-port=9223 "$@"
    '';
  my-google =
    prev.writeScriptBin "google-chrome"
    ''      #!/usr/bin/env bash
            ${prev.google-chrome}/bin/google-chrome-stable --remote-debugging-port=9224 "$@"
    '';
  my-chromium =
    prev.writeScriptBin "chromium"
    ''      #!/usr/bin/env bash
            ${prev.ungoogled-chromium}/bin/chromium --remote-debugging-port=9225 "$@"
    '';
}
