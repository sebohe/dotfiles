vim.opt.shortmess:append "sI"
function t(str)
    -- Adjust boolean arguments as needed
    return vim.api.nvim_replace_termcodes(str, true, true, true)
end


-- filetype plugin indent on " for more info try :h filetype-plugin
-- Remap some common misstypes due to fast fingers
vim.cmd [[
  command WQ wq
  command Wq wq
  command W w
  command Q q
  " Write as sudo
  cmap w!! w !sudo tee > /dev/null %
]]
-- Disable some built-in plugins we don't want
local disabled_built_ins = {
  'man',
  'matchparen',
  'shada_plugin',
  "gzip",
  "zip",
  "zipPlugin",
  "tar",
  "tarPlugin",
  "getscript",
  "getscriptPlugin",
  "vimball",
  "vimballPlugin",
  "2html_plugin",
  "logipat",
  "rrhelper",
  "spellfile_plugin",
  "matchit",
}
for _, plugin in pairs(disabled_built_ins) do
    vim.g["loaded_" .. plugin] = 1
end

vim.g.mapleader = ' '

local set = vim.opt
-- https://www.johnhawthorn.com/2012/09/vi-escape-delays/
set.timeoutlen = 400
set.guifont = 'Fira Code:h11'
set.encoding = 'utf-8'
set.tabstop = 2
set.shiftwidth = 2
set.softtabstop = 2
set.expandtab = true
set.ff = 'unix'

-- Line numbers
set.number = true
set.relativenumber = true
-- set.eol = false
vim.cmd [[
augroup linenumbers
  autocmd!
  autocmd BufEnter *    :set relativenumber
  autocmd BufLeave *    :set number norelativenumber
  autocmd WinEnter *    :set relativenumber
  autocmd WinLeave *    :set number norelativenumber
  autocmd InsertEnter * :set number norelativenumber
  autocmd InsertLeave * :set relativenumber
  autocmd FocusLost *   :set number norelativenumber
  autocmd FocusGained * :set relativenumber
augroup END
]]
vim.cmd [[
  autocmd FileType markdown setlocal spell spelllang=en_us
  autocmd BufNewFile,BufRead *.md set filetype=markdown
  autocmd FileType markdown set conceallevel=2
]]

-- enable mouse support
set.mouse = 'a'
set.undofile = true
set.undodir = vim.fn.getenv('HOME') .. '/.cache/nvim/undodir'
local install_path = vim.fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  packer_bootstrap = vim.fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end


if vim.fn.has('termguicolors') == 1 then
  set.termguicolors = true
end

local fire = vim.api.nvim_eval('exists("g:started_by_firenvim")')
if (fire == 0) then
  print("NORMAL")
  vim.cmd('colorscheme gruvbox')
  vim.g.gruvbox_transp_bg = 1
  vim.g.gruvbox_italicize_strings = 0
  require ('plugins')
  require ('explorer')
  require ("lsp_config")
  require ('line')
else
  print("FIREVIM")
  set.cmdheight=0
  set.showmode=false
  set.ruler=false
  set.showcmd=false
  set.laststatus=0
end
