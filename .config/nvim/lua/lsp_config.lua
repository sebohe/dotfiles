
-- Preview window + menu for autocompletions
vim.opt.completeopt = 'menu,menuone,noselect'
vim.opt.signcolumn = 'yes'

local t = function(str)
    return vim.api.nvim_replace_termcodes(str, true, true, true)
end

local function buf_set_keymap(bufnr, ...) 
  if bufnr == nil then
    bufnr = 0
  end
  vim.api.nvim_buf_set_keymap(bufnr, ...)
end

local function buf_set_option(...)
  if bufnr == nil then
    bufnr = 0
  end
  vim.api.nvim_buf_set_option(bufnr, ...)
end

local on_attach = function(client, bufnr)
  vim.opt.updatetime = 200

  -- Mappings.
  local opts = { noremap=true, silent=true }
  buf_set_keymap(bufnr, 'n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap(bufnr, 'n', 'ga', '<Cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  buf_set_keymap(bufnr, 'n', 'gr', '<Cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap(bufnr, 'n', 'gi', '<Cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap(bufnr, 'n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap(bufnr, 'n', '<leader>k', '<Cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap(bufnr, 'n', '<leader>D', '<Cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap(bufnr, 'n', '<leader>rn', '<Cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap(bufnr, 'n', '[g', '<Cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap(bufnr, 'n', ']g', '<Cmd>lua vim.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap(bufnr, 'n', '<leader>e', '<Cmd>lua vim.diagnostic.open_float()<CR>', opts)
  buf_set_keymap(bufnr, 'n', '<leader>q', '<Cmd>lua vim.diagnostic.set_loclist()<CR>', opts)

  -- Set some keybinds conditional on server capabilities
  if client.server_capabilities.document_formatting then
    vim.api.nvim_create_autocmd("BufWritePre", {
      pattern = "*",
      callback = function()
        vim.lsp.buf.format({timeout_ms = 4000, async = false})
      end
    })
    vim.cmd [[
      autocmd BufWritePre *.go :silent! lua require('go.format').gofmt()
    ]]
  end

  -- Set autocommands conditional on server_capabilities
  if client.server_capabilities.document_highlight then
    vim.cmd [[
      hi LspReferenceRead cterm=bold ctermbg=DarkMagenta guibg=LightYellow
      hi LspReferenceText cterm=bold ctermbg=DarkMagenta guibg=LightYellow
      hi LspReferenceWrite cterm=bold ctermbg=DarkMagenta guibg=LightYellow
      augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
      augroup END
    ]]
  end
end

function goimports(timeoutms)
  print("called go imports")
  local context = { source = { organizeImports = true } }
  vim.validate { context = { context, "t", true } }

  local params = vim.lsp.util.make_range_params()
  params.context = context

  -- See the implementation of the textDocument/codeAction callback
  -- (lua/vim/lsp/handler.lua) for how to do this properly.
  local result = vim.lsp.buf_request_sync(0, "textDocument/codeAction", params, timeout_ms)
  if not result or next(result) == nil then return end
  local actions = result[1].result
  if not actions then return end
  local action = actions[1]

  -- textDocument/codeAction can return either Command[] or CodeAction[]. If it
  -- is a CodeAction, it can have either an edit, a command or both. Edits
  -- should be executed first.
  if action.edit or type(action.command) == "table" then
    if action.edit then
      vim.lsp.util.apply_workspace_edit(action.edit)
    end
    if type(action.command) == "table" then
      vim.lsp.buf.execute_command(action.command)
    end
  else
    vim.lsp.buf.execute_command(action)
  end
end

local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities.textDocument.completion.completionItem.resolveSupport = {
  properties = { "documentation", "detail", "additionalTextEdits" },
}

local lsp = require('lspconfig')
lsp.gopls.setup{
	cmd = {'gopls'},
	capabilities = capabilities,
  on_attach = function(client, bufnr)
    client.server_capabilities.document_formatting = true
    client.server_capabilities.document_range_formatting = false
    on_attach(client, bufnr)
  end,
  settings = {
    gopls = {
      experimentalPostfixCompletions = true,
      analyses = {
        unusedparams = true,
        shadow = true,
     },
     staticcheck = true,
    },
  },
}
require('go').setup()

lsp.yamlls.setup{}
require('rust-tools').setup({
    tools = { -- rust-tools options
        autoSetHints = true,
        inlay_hints = {
            show_parameter_hints = true,
            parameter_hints_prefix = "",
            other_hints_prefix = "",
        },
    },
    --dap = require'debugging',
    server = {
        capabilities = capabilities,
        on_attach = function(client, bufnr)
          client.server_capabilities.document_formatting = true
          client.server_capabilities.document_range_formatting = false
          vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
          on_attach(client, bufnr)
        end,
        settings = {
          ["rust-analyzer"] = {
            checkOnSave = {
                command = "clippy"
            },
            lens = {
              enable = true,
            },
            diagnostics = {
              enable = true,
            },
            assist = {
                importGranularity = "module",
                importPrefix = "self",
            },
            cargo = {
              autoReload = true,
              watch = true,
              allFeatures = true,
              loadOutDirsFromCheck = true
            },
            procMacro = {
                enable = true
            },
          }
        }
    },
})

-------------
-- Typescript
-------------
require("typescript").setup({
    disable_commands = false, -- prevent the plugin from creating Vim commands
    disable_formatting = true, -- disable tsserver's formatting capabilities
    debug = false, -- enable debug logging for commands
    server = { -- pass options to lspconfig's setup method
        on_attach = function(client, bufnr)
          client.server_capabilities.document_formatting = false
          client.server_capabilities.document_range_formatting = false
          on_attach(client, bufnr)
        end,
        capabilities = capabilities,
    },
})

local null_ls = require("null-ls")
local prefer_local = {prefer_local = "node_modules/.bin"}
null_ls.setup({
    sources = {
        null_ls.builtins.formatting.prettierd.with(prefer_local),
        --null_ls.builtins.formatting.eslint_d.with(prefer_local),
        null_ls.builtins.formatting.alejandra,
    },
    capabilities = capabilities,
    on_attach = function(client, bufnr)
      client.server_capabilities.document_formatting = true
      client.server_capabilities.document_range_formatting = true
      on_attach(client, bufnr)
    end,
})


-------------
-- Completion
-------------

local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local check_back_space = function()
    local col = vim.fn.col('.') - 1
    if col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
        return true
    else
        return false
    end
end

local feedkey = function(key, mode)
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
end

local lspkind = require 'lspkind'
require("nvim-autopairs").setup()
local luasnip = require('luasnip')
local cmp_autopairs = require("nvim-autopairs.completion.cmp")
local cmp = require('cmp')
cmp.event:on(
  'confirm_done',
  cmp_autopairs.on_confirm_done()
)
cmp.setup({
  completion = {
    autocomplete = {
      cmp.TriggerEvent.TextChanged,
      cmp.TriggerEvent.InsertEnter,
    },
    completeopt = "menuone,noinsert,noselect",
    keyword_length = 0,
  },
  preselect = cmp.PreselectMode.None,
  snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end,
	},
  window = {
    completion = cmp.config.window.bordered(),
  },
  formatting = {
    format = function(entry, vim_item)
      vim_item.kind = lspkind.presets.default[vim_item.kind] .. " " .. vim_item.kind
      vim_item.abbr = vim.fn.strcharpart(vim_item.abbr, 0, 50) -- hack to clamp cmp-cmdline-history len
      vim_item.menu = ({
        copilot = "[COP]",
        cmdline_history = "[HIST]",
        cmdline = "[CMD]",
        fuzzy_path = "[PATH]",
        buffer = "[BUFF]",
      })[entry.source.name]
      return vim_item
    end,
	},
  sources = cmp.config.sources({
    { name = "copilot", group_index = 2 },
		{ name = "luasnip", max_item_count = 2},
		{ name = "nvim_lsp" },
		{ name = "buffer", keyword_length = 3 },
		{ name = "path"},
		{ name = "nvim_lsp_signature_help" },
	}),
  mapping = cmp.mapping.preset.insert({
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif has_words_before() then
        cmp.complete()
      else
        fallback()
      end
    end, { "i", "s" }),

    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { "i", "s" }),
    ["<esc>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.abort()
      else
        fallback()
      end
    end, {'i'}),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-u>'] = cmp.mapping.scroll_docs(4),
    ['<CR>'] = cmp.mapping.confirm({ select = false }),
  }),
})
