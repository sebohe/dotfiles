#!/usr/bin/env bash
function dirMaker() {
  for x in $1; do
    mkdir -vp $HOME/$x
  done
}

dirMaker "$(find .config/ -type d)"
dirMaker "$(find .local/ -type d)"

files=$(find . ! -iwholename '*.git*' -type f | grep -wv 'dwm\|setup\.sh\|nixos\|README')
for x in $files; do
  echo "Linking file: $x -> $HOME/$x"
  ln -sf `pwd`/$x $HOME/$x
done
